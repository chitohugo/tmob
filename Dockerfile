FROM python:3.7-slim
ENV PYTHONUNBUFFERED 1
RUN apt-get -y update
RUN apt-get install -y python3-dev default-libmysqlclient-dev build-essential 
RUN python3 -m pip install --upgrade pip
RUN mkdir /project
WORKDIR /project
COPY requirements.txt /project/
RUN pip install -r requirements.txt
COPY . /project/
