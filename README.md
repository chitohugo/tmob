# API for Proof

## Running with docker

### Pre-requisites:
- docker
- docker-compose

### Steps
1. Build with `docker-compose build`
2. Run with `docker-compose up`

### How to use
1. Access container tmob_web_1 `docker exec -it tmob_web_1 bash`
2. In terminal run: python manage.py migrate, python manage.py createsuperuser
3. Access a `http://0.0.0.0:8000/admin/`
4. Add or modify data to the model
5. With client rest `http://0.0.0.0:8000/redirect` send by query params the key.

### Example: `http://0.0.0.0:8000/redirect?key=facebook`