from django.contrib import admin


# Register your models here.
from proof.models import Redirect


class RedirectAdmin(admin.ModelAdmin):
    # readonly_fields = ['id', 'key', 'url']
    list_display = ['key', 'url', 'created_at', 'updated_at', 'active']


admin.site.register(Redirect, RedirectAdmin)
