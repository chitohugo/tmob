from django.db.models.signals import post_save
from django.dispatch import receiver
from proof.models import Redirect
from django.core.cache import cache


@receiver(post_save, sender=Redirect)
def update_redirect(sender, instance, created, **kwargs):
    if not created:
        redirects = Redirect.objects.all().filter(active=True)
        for redirect in redirects:
            cache.set(redirect.key, {
                'id': redirect.url,
                'key': redirect.key,
                'url': redirect.url,
                'active': redirect.active,
                'created_at': redirect.created_at,
                'updated_at': redirect.updated_at,
            },
                      timeout=None)


def find_key(key):
    data = cache.get(key)
    return data['url']
