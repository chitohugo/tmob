from django.db import models


# Create your models here.
class Redirect(models.Model):
    key = models.CharField(max_length=255)
    url = models.CharField(max_length=255)
    active = models.BooleanField(default=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

