from django.http import Http404
from rest_framework.response import Response
from rest_framework.views import APIView
from proof.models import Redirect
from proof.signals import find_key


class RedirectView(APIView):

    def get_object(self, key):
        try:
            instance = Redirect.objects.get(key=key)
            url = find_key(instance.key)
            return {'key': key, 'url': url}
        except Redirect.DoesNotExist:
            raise Http404

    def get(self, request):
        key = request.query_params['key']
        redirect = self.get_object(key)
        return Response(redirect)
