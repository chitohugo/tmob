from django.urls import path
from proof import views

urlpatterns = [
    path('redirect/', views.RedirectView.as_view()),
]
