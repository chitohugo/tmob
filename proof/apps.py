from django.apps import AppConfig


class ProofConfig(AppConfig):
    name = 'proof'

    def ready(self):
        import proof.signals
